package com.shortcircuit.splatoon.commands;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.Arena;
import com.shortcircuit.splatoon.game.statistics.SplatSign;
import com.shortcircuit.splatoon.player.SquidClass;
import com.shortcircuit.utils.collect.ConcurrentArrayList;
import com.shortcircuit.utils.bukkit.command.BaseCommand;
import com.shortcircuit.utils.bukkit.command.CommandType;
import com.shortcircuit.utils.bukkit.command.exceptions.CommandException;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

import java.util.LinkedList;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class RulesCommand extends BaseCommand<CommandSender> {
	public RulesCommand(Plugin plugin) {
		super(plugin);
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.ANY;
	}

	@Override
	public String getCommandName() {
		return "splatooninfo";
	}

	@Override
	public String[] getCommandAliases() {
		return new String[]{"splatinfo", "sinfo"};
	}

	@Override
	public String getCommandPermission() {
		return "splatoon.info";
	}

	@Override
	public String[] exec(CommandSender sender, String command, ConcurrentArrayList<String> args) throws CommandException {
		if (args.size() > 0) {
			switch (args.get(0).toLowerCase()) {
				case "arenas":
					StringBuilder info_builder = new StringBuilder(ChatColor.GOLD + "List of arenas (")
							.append(ChatColor.GREEN).append(SplatSign.STATES[0].toLowerCase()).append(ChatColor.GOLD).append(", ")
							.append(ChatColor.YELLOW).append(SplatSign.STATES[1].toLowerCase()).append(ChatColor.GOLD).append(", ")
							.append(ChatColor.RED).append(SplatSign.STATES[2].toLowerCase()).append(ChatColor.GOLD).append("):\n");
					int arenas_displayed = 0;
					LinkedList<Arena> arenas = Splatoon.getInstance().getArenaManager().getArenas();
					for (Arena arena : arenas) {
						ChatColor color = ChatColor.GREEN;
						if (arena.getMatchHandler().hasMatch()) {
							if (arena.getMatchHandler().isMatchStarted()) {
								color = ChatColor.RED;
							}
							else {
								color = ChatColor.YELLOW;
							}
						}
						info_builder.append(color).append(arena.getID());
						if (arenas_displayed < arenas.size() - 1) {
							info_builder.append(ChatColor.GOLD);
							String suffix = ((++arenas_displayed % 16 == 0) ? ",\n" : ", ");
							info_builder.append(suffix);
						}
					}
					return info_builder.toString().split("\n");
				case "classes":
					LinkedList<String> message = new LinkedList<>();
					message.add(ChatColor.GOLD + "---------- " + ChatColor.AQUA + "Squid classes" + ChatColor.GOLD + " ----------");
					for (SquidClass squid_class : SquidClass.getRegisteredClasses()) {
						message.add(ChatColor.GOLD + "---------- " + squid_class.getClassName() + ChatColor.GOLD + " ----------");
						message.add(ChatColor.GOLD + squid_class.getDescription());
					}
					return message.toArray(new String[0]);
				case "commands":
					return new String[]{
							ChatColor.GOLD + "---------- " + ChatColor.AQUA + "Commands" + ChatColor.GOLD + " ----------",
							ChatColor.GOLD + "Type \"/<command> help\" for more detailed information",
							"- /joinsplatoon",
							"- /leavesplatoon",
							"- /splatoonarena",
							"- /splatooninfo",
							"- /splatoonmatch",
							"- /splatoonsign",
							"- /splatoonspawn",
							"- /votestart"
					};
				case "rules":
					String[] config_rules = Splatoon.getInstance().getLangConfig().getNode("info.rules", String[].class,
							new String[]{
									"The objective of Splatoon is to cover as much of the arena with your color as possible within three minutes.",
									"This is achieved by right- or left-clicking with your tool to shoot ink at a surface.",
									"Switching your hand to an empty slot will transform you into a squid, giving you a speed boost while on your own color of ink",
									"As a squid, you may also climb walls that are covered in your color of ink. However, you will not be able to spread ink as a squid.",
									"Switching your hand back to your tool returns you to normal and allows you to spread ink again.",
									"Coming into contact with ink that is an opponent's color will slowly damage you.",
									"In addition, direct contact with ink sprayed by another player will damage you."
							});
					String[] rules = new String[config_rules.length + 1];
					rules[0] = ChatColor.GOLD + "---------- " + ChatColor.AQUA + "Splatoon" + ChatColor.GOLD + " ----------";
					boolean color_toggle = false;
					for (int i = 0; i < config_rules.length; i++) {
						rules[i + 1] = (color_toggle ? ChatColor.GOLD : ChatColor.YELLOW) + config_rules[i];
						color_toggle ^= true;
					}
					return rules;
			}
		}
		return new String[]{
				ChatColor.GOLD + "---------- " + ChatColor.AQUA + "Topics" + ChatColor.GOLD + " ----------",
				"- Arenas",
				"- Classes",
				"- Commands",
				"- Rules"
		};

	}

	@Override
	public String[] getCommandUsage() {
		return new String[]{"/<command> <topic>"};
	}

	@Override
	public String[] getCommandDescription() {
		return new String[]{
				"View information about the game"
		};
	}
}