package com.shortcircuit.splatoon.game.statistics;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.Arena;
import com.shortcircuit.splatoon.player.SquidClass;
import com.shortcircuit.splatoon.player.TeamColor;
import com.shortcircuit.utils.bukkit.LocationWrapper;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;

import java.util.ArrayList;
import java.util.Random;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class LobbySign {
	private LocationWrapper sign_location = null;
	private transient SquidClass join_class_instance;
	private String join_class;
	private TeamColor join_color;
	private final int arena_id;

	public LobbySign(Location location, int arena_id, SquidClass join_class, TeamColor join_color) {
		this.sign_location = new LocationWrapper(location);
		this.arena_id = arena_id;
		this.join_class_instance = join_class;
		this.join_color = join_color;
		update();
	}

	public Sign getSign() {
		if (sign_location == null || sign_location.getLocation() == null) {
			return null;
		}
		try {
			Block block = sign_location.getLocation().getBlock();
			if (block.getState() instanceof Sign) {
				return (Sign) block.getState();
			}
		}
		catch (Exception e) {
			// Do nothing
		}
		return null;
	}

	public Location getSignLocation() {
		update();
		return sign_location.getLocation();
	}

	public void setSignLocation(Location location) {
		this.sign_location = new LocationWrapper(location);
		update();
	}

	public void update() {
		Sign sign = getSign();
		if (sign == null) {
			return;
		}
		sign.setLine(0, "Arena #" + arena_id);
		if (getJoinClass() != null) {
			sign.setLine(1, "[" + getJoinClass().getClassName() + ChatColor.BLACK + "]");
		}
		else {
			sign.setLine(1, "");
		}
		if (join_color != null) {
			sign.setLine(2, "[" + join_color.getTeamName() + ChatColor.BLACK + "]");
		}
		else {
			sign.setLine(2, "");
		}
		sign.setLine(3, "Click to join");
		sign.update();
	}

	public SquidClass getJoinClass() {
		if (join_class == null) {
			Arena arena = Splatoon.getInstance().getArenaManager().getArena(arena_id);
			if (arena.getMatchHandler().hasMatch()) {
				return arena.getMatchHandler().getCurrentMatch().getDeficientClass();
			}
			ArrayList<SquidClass> registered_classes = SquidClass.getRegisteredClasses();
			return registered_classes.get(new Random(System.currentTimeMillis()).nextInt(registered_classes.size()));
		}
		if (join_class_instance == null) {
			join_class_instance = SquidClass.getSquidClass(join_class);
		}
		if (join_class_instance == null) {
			setJoinClass(null);
		}
		return join_class_instance;
	}

	public void setJoinClass(SquidClass join_class) {
		this.join_class_instance = join_class;
		if (join_class != null) {
			this.join_class = ChatColor.stripColor(join_class.getClassName()).toLowerCase();
		}
		update();
	}

	public TeamColor getJoinColor() {
		if (join_color == null) {
			Arena arena = Splatoon.getInstance().getArenaManager().getArena(arena_id);
			if (arena.getMatchHandler().hasMatch()) {
				return arena.getMatchHandler().getCurrentMatch().getDeficientTeam();
			}
			return TeamColor.values()[new Random(System.currentTimeMillis()).nextInt(TeamColor.values().length)];
		}
		return join_color;
	}

	public void setJoinColor(TeamColor join_color) {
		this.join_color = join_color;
		update();
	}
}