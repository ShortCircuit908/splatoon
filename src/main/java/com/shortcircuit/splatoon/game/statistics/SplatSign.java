package com.shortcircuit.splatoon.game.statistics;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.Match;
import com.shortcircuit.splatoon.player.TeamColor;
import com.shortcircuit.utils.bukkit.LocationWrapper;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class SplatSign {
	private static final String[] FORMAT = Splatoon.getInstance().getJsonConfig().getNode("sign_format", String[].class,
			new String[]{"Arena #%arena%", "%status%", "Time remaining", "%time%"});
	public static final String[] STATES = new String[]{
			ChatColor.translateAlternateColorCodes('&', Splatoon.getInstance().getLangConfig().getNode("match.state.open", String.class, "&aOPEN")),
			ChatColor.translateAlternateColorCodes('&', Splatoon.getInstance().getLangConfig().getNode("match.state.queued", String.class, "&eMATCH QUEUED")),
			ChatColor.translateAlternateColorCodes('&', Splatoon.getInstance().getLangConfig().getNode("match.state.started", String.class, "&4IN PROGRESS")),
			ChatColor.translateAlternateColorCodes('&', Splatoon.getInstance().getLangConfig().getNode("match.state.closed", String.class, "&4CLOSED"))
	};
	private LocationWrapper sign_location = null;
	private final long arena_id;
	private int last_state = 0;
	private int players = 0;
	private int time = 0;
	private int[] scores = {0, 0, 0, 0};

	public SplatSign(Location location, long arena_id) {
		this.sign_location = new LocationWrapper(location);
		this.arena_id = arena_id;
	}

	public String formatState() {
		return STATES[Math.min(last_state, 3)];
	}

	public Sign getSign() {
		if (sign_location == null || sign_location.getLocation() == null) {
			return null;
		}
		try {
			Block block = sign_location.getLocation().getBlock();
			if (block.getState() instanceof Sign) {
				return (Sign) block.getState();
			}
		}
		catch (Exception e) {
			// do nothing
		}
		return null;
	}

	public Location getSignLocation() {
		if (sign_location == null) {
			return null;
		}
		return sign_location.getLocation();
	}

	public void updateMatchState(int state) {
		this.last_state = state;
		if (state != 2) {
			time = 0;
		}
		update();
	}

	public void updatePlayers(int players) {
		this.players = players;
		update();
	}

	public void updateTime(int time) {
		this.time = time;
		update();
	}

	public void setSignLocation(Location location) {
		if (location == null) {
			sign_location = null;
		}
		else {
			sign_location = new LocationWrapper(location);
		}
		update();
	}

	public void updateScores(int score, TeamColor team) {
		switch (team) {
			case RED:
				scores[0] = score;
				break;
			case YELLOW:
				scores[1] = score;
				break;
			case GREEN:
				scores[2] = score;
				break;
			case BLUE:
				scores[3] = score;
				break;
		}
		update();
	}

	public void update() {
		Sign sign = getSign();
		if (sign == null) {
			return;
		}
		for (int i = 0; i < 4; i++) {
			sign.setLine(i, i > 1 && last_state > 2 ? "" : formatLine(i));
		}
		sign.update();
	}

	private String formatScores() {
		return new StringBuilder().append(ChatColor.RED).append(scores[0])
				.append("    ").append(ChatColor.YELLOW).append(scores[1])
				.append("    ").append(ChatColor.GREEN).append(scores[2])
				.append("    ").append(ChatColor.BLUE).append(scores[3])
				.toString();
	}


	private String formatLine(int line_number) {
		return ChatColor.translateAlternateColorCodes('&', FORMAT[line_number]
				.replace("%arena%", arena_id + "")
				.replace("%status%", formatState())
				.replace("%time%", time + "")
				.replace("%pct%", players + "")
				.replace("%pmax%", Match.MAX_PLAYERS + "")
				.replace("%scores%", formatScores()));

	}
}
