package com.shortcircuit.splatoon.game.powerups;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.Arena;
import com.shortcircuit.splatoon.game.Match;
import com.shortcircuit.splatoon.game.event.PlayerPaintBlockEvent;
import com.shortcircuit.splatoon.listeners.SplatListener;
import com.shortcircuit.splatoon.player.Inkling;
import com.shortcircuit.splatoon.player.TeamColor;
import com.shortcircuit.splatoon.util.Utils;
import com.shortcircuit.utils.bukkit.BukkitUtils;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

import java.util.Random;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class Infection implements Runnable {
	private static final Random RANDOM = new Random(System.currentTimeMillis());
	private final Inkling inkling;
	private final Block origin;
	private final double infection_chance;
	private final Arena parent_arena;
	private final TeamColor team;
	private final boolean originator;
	private final int task_id;

	public Infection(Inkling inkling, Block origin, double infection_chance, Arena parent_arena, TeamColor team) {
		this(inkling, origin, infection_chance, parent_arena, team, true);
	}

	private Infection(Inkling inkling, Block origin, double infection_chance, Arena parent_arena, TeamColor team, boolean originator) {
		this.inkling = inkling;
		this.origin = origin;
		this.infection_chance = infection_chance;
		this.parent_arena = parent_arena;
		this.team = team;
		this.originator = originator;
		task_id = Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Splatoon.getInstance(), this, RANDOM.nextInt(120) + 21);
	}

	@Override
	public void run() {
		if (!canInfect(origin) || !parent_arena.getMatchHandler().isMatchStarted()) {
			return;
		}
		Match match = parent_arena.getMatchHandler().getCurrentMatch();
		byte old_data = origin.getData();
		byte new_data = team.getColorData();
		if (old_data == new_data) {
			return;
		}
		origin.setData(new_data);
		origin.getState().update();
		TeamColor decrement = TeamColor.getTeam(old_data);
		if (decrement != null) {
			match.getStatistics().decrement(decrement);
		}
		inkling.removeAmmunition(1);
		match.getStatistics().increment(team);
		match.getStatistics().update();
		Bukkit.getServer().getPluginManager().callEvent(new PlayerPaintBlockEvent(inkling, match, origin, TeamColor.getTeam(old_data), team));
		for (BlockFace modifier : SplatListener.FACE_MODIFIERS) {
			for (BlockFace face : SplatListener.CHECK_FACES) {
				if (RANDOM.nextDouble() <= infection_chance) {
					Block neighbor = BukkitUtils.getNeighboringBlock(origin.getLocation(), modifier, face).getBlock();
					if (!canInfect(neighbor)) {
						continue;
					}
					new Infection(inkling, neighbor, infection_chance - RANDOM.nextDouble(), parent_arena, team, false);
				}
			}
		}
	}

	private boolean canInfect(Block block) {
		return block != null
				&& inkling.getAmmunition() >= 1
				&& (block.getType().isSolid() || originator)
				&& ((Utils.getPaintableSurfaces().contains(block.getType()) && block.getData() != team.getColorData()) || originator)
				&& Utils.isInArena(block.getLocation(), parent_arena);
	}
}
