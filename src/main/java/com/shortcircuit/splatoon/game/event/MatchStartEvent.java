package com.shortcircuit.splatoon.game.event;

import com.shortcircuit.splatoon.game.Match;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 11/16/2015
 */
public class MatchStartEvent extends SplatoonEvent {
	private final Match match;
	private final Cause cause;

	public MatchStartEvent(Match match, Cause cause) {
		this.match = match;
		this.cause = cause;
	}

	public Match getMatch() {
		return match;
	}

	public Cause getCause() {
		return cause;
	}

	public enum Cause {
		TIMER,
		COMMAND,
		VOTE
	}
}