package com.shortcircuit.splatoon.game.event;

import com.shortcircuit.splatoon.game.Match;
import com.shortcircuit.splatoon.player.Inkling;
import com.shortcircuit.splatoon.player.TeamColor;
import org.bukkit.block.Block;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 11/16/2015
 */
public class PlayerPaintBlockEvent extends SplatoonEvent {
	private final Inkling inkling;
	private final Match match;
	private final Block block;
	private final TeamColor old_color;
	private final TeamColor new_color;

	public PlayerPaintBlockEvent(Inkling inkling, Match match, Block block, TeamColor old_color, TeamColor new_color) {
		this.inkling = inkling;
		this.match = match;
		this.block = block;
		this.old_color = old_color;
		this.new_color = new_color;
	}

	public Inkling getInkling() {
		return inkling;
	}

	public Match getMatch() {
		return match;
	}

	public Block getBlock() {
		return block;
	}

	public TeamColor getOldColor() {
		return old_color;
	}

	public TeamColor getNewColor() {
		return new_color;
	}
}