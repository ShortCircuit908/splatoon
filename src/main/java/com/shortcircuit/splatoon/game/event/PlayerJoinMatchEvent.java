package com.shortcircuit.splatoon.game.event;

import com.shortcircuit.splatoon.game.Match;
import com.shortcircuit.splatoon.player.Inkling;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 11/16/2015
 */
public class PlayerJoinMatchEvent extends SplatoonEvent {
	private final Inkling inkling;
	private final Match match;

	public PlayerJoinMatchEvent(Inkling inkling, Match match) {
		this.inkling = inkling;
		this.match = match;
	}

	public Inkling getInkling() {
		return inkling;
	}

	public Match getMatch() {
		return match;
	}
}
