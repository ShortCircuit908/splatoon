package com.shortcircuit.splatoon.game.timers;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.Match;
import com.shortcircuit.splatoon.player.*;
import com.shortcircuit.splatoon.util.Utils;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;

/**
 * @author ShortCircuit908
 */
public class DamageTimer implements Runnable {
	private static final Function<? super Double, Double> ZERO = Functions.constant(Double.valueOf(-0.0D));
	private final Match match;
	private final int task_id;

	public DamageTimer(Match match) {
		this.match = match;
		this.task_id = Bukkit.getScheduler().scheduleSyncRepeatingTask(Splatoon.getInstance(), this, 0, 20);
	}

	@Override
	public void run() {
		if (!match.hasStarted()) {
			return;
		}
		for (Inkling inkling : match.getInklings()) {
			TeamColor team = inkling.getTeam();
			byte team_color = team.getColorData();
			Block standing_on = inkling.getPlayer().getLocation().subtract(0, 1, 0).getBlock();
			if (standing_on == null
					|| !Utils.getPaintableSurfaces().contains(standing_on.getType())
					|| !Utils.isTeamColorData(standing_on.getData())) {
				continue;
			}
			if (inkling.isDisguised() || !Splatoon.getInstance().getJsonConfig().getNode("ink_regen_requires_disguise", boolean.class, true)) {
				if (standing_on.getData() == team_color) {
					if (inkling.getSquidClass() instanceof Shooter) {
						inkling.addAmmunition(Splatoon.getInstance().getJsonConfig().getNode("class.shooter.ink_regen_per_second", int.class, 8));
					}
					else if (inkling.getSquidClass() instanceof Roller) {
						inkling.addAmmunition(Splatoon.getInstance().getJsonConfig().getNode("class.roller.ink_regen_per_second", int.class, 8));
					}
					else if (inkling.getSquidClass() instanceof Charger) {
						inkling.addAmmunition(Splatoon.getInstance().getJsonConfig().getNode("class.charger.ink_regen_per_second", int.class, 8));
					}
					else if (inkling.getSquidClass() instanceof Carrier) {
						inkling.addAmmunition(Splatoon.getInstance().getJsonConfig().getNode("class.carrier.ink_regen_per_second", int.class, 8));
					}
					continue;
				}
				else {
					inkling.addAmmunition(1);
				}
			}
			//Map<EntityDamageEvent.DamageModifier, Double> modifiers = new HashMap<>(1);
			//modifiers.put(EntityDamageEvent.DamageModifier.BASE, 2.0);
			//Map<EntityDamageEvent.DamageModifier, Function<? super Double, Double>> other_map = new HashMap<>(1);
			//other_map.put(EntityDamageEvent.DamageModifier.BASE, ZERO);
			//EntityDamageEvent event = new EntityDamageEvent(inkling.getPlayer(), EntityDamageEvent.DamageCause.CUSTOM, modifiers, other_map);
			//Bukkit.getServer().getPluginManager().callEvent(event);
			//if (!event.isCancelled()) {
			//	inkling.getPlayer().damage(event.getFinalDamage());
			//}
			if (inkling.getPlayer().getHealth() <= inkling.getPlayer().getMaxHealth() * 0.10) {
				inkling.respawn();
			}
			else if(team_color != standing_on.getData()){
				inkling.getPlayer().damage(inkling.getPlayer().getMaxHealth() * 0.10);
			}
		}
	}

	public void cancel() {
		Bukkit.getServer().getScheduler().cancelTask(task_id);
	}
}
