package com.shortcircuit.splatoon.game;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.player.Inkling;
import com.shortcircuit.splatoon.util.AnnouncementHandler;
import org.bukkit.Location;

import java.io.File;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.UUID;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class ArenaManager {
	private final LinkedList<Arena> arenas = new LinkedList<>();

	public void addArena(Arena arena) {
		arenas.add(arena);
	}

	public void removeArena(int id) {
		Arena arena = getArena(id);
		if (arena != null) {
			arenas.remove(arena);
			if (arena.getMatchHandler().hasMatch()) {
				if (arena.getMatchHandler().isMatchStarted()) {
					arena.getMatchHandler().getCurrentMatch().getTimer().stop();
				}
				else {
					arena.getMatchHandler().getCurrentMatch().stopMatch();
				}
				AnnouncementHandler.announceArenaOnly("arena.remove", arena);
			}
			arena.getStatusSign().updateMatchState(3);
			File arena_schematic = new File(Splatoon.getInstance().getDataFolder() + "/arenas/" + id + ".schematic");
			if (arena_schematic.exists()) {
				arena_schematic.delete();
			}
		}
	}

	public boolean arenaExists(int id) {
		return getArena(id) != null;
	}

	public LinkedList<Arena> getArenas() {
		return arenas;
	}

	public Arena getArena(int id) {
		for (Arena arena : arenas) {
			if (arena.getID() == id) {
				return arena;
			}
		}
		return null;
	}

	public Inkling getInkling(UUID id) {
		Inkling inkling;
		for (Arena arena : arenas) {
			if (arena.getMatchHandler().hasMatch()) {
				inkling = arena.getMatchHandler().getCurrentMatch().getInkling(id);
				if (inkling != null) {
					return inkling;
				}
			}
		}
		return null;
	}

	public Arena getArenaContainingInkling(UUID id) {
		for (Arena arena : arenas) {
			if (arena.getMatchHandler().hasMatch() && arena.getMatchHandler().getCurrentMatch().getInkling(id) != null) {
				return arena;
			}
		}
		return null;
	}

	public Arena getArenaByLocation(Location inside_arena) {
		for (Arena arena : arenas) {
			if (!arena.getWorld().getUID().equals(inside_arena.getWorld().getUID())) {
				continue;
			}
			if (inside_arena.toVector().isInAABB(arena.getMinPos().toVector(), arena.getMaxPos().add(1.0, 1.0, 1.0).toVector())) {
				return arena;
			}
		}
		return null;
	}

	public void organizeArenas() {
		Collections.sort(arenas, new ArenaSorter());
	}

	private class ArenaSorter implements Comparator<Arena> {
		@Override
		public int compare(Arena arena_1, Arena arena_2) {
			if (arena_1.getID() < arena_2.getID()) {
				return -1;
			}
			if (arena_1.getID() > arena_2.getID()) {
				return 1;
			}
			return 0;
		}
	}
}
