package com.shortcircuit.splatoon.player;

import com.shortcircuit.splatoon.Splatoon;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public abstract class SquidClass implements Listener {
	private static final LinkedList<SquidClass> registered_classes = new LinkedList<>();

	private final String name;
	private boolean registered = false;

	public SquidClass(String name) {
		this.name = name;
	}

	public abstract ItemStack cloneGunItem(TeamColor team);

	public abstract String getDescription();

	public ItemStack[] cloneAdditionalItems(TeamColor team) {
		return null;
	}

	public final void register() {
		if (registered) {
			throw new IllegalStateException("Squid class is already registered");
		}
		synchronized (registered_classes) {
			SquidClass conflict = getSquidClass(name);
			if (conflict != null) {
				throw new IllegalArgumentException("Squid class " + conflict.getClassName() + " already exists");
			}
			registered_classes.add(this);
			registered = true;
			Bukkit.getServer().getPluginManager().registerEvents(this, Splatoon.getInstance());
		}
	}

	public static ArrayList<SquidClass> getRegisteredClasses() {
		synchronized (registered_classes) {
			return new ArrayList<>(registered_classes);
		}
	}

	public String getClassName() {
		return name;
	}

	public static SquidClass getSquidClass(String name) {
		synchronized (registered_classes) {
			name = ChatColor.stripColor(name);
			for (SquidClass squid_class : registered_classes) {
				if (ChatColor.stripColor(squid_class.getClassName()).equalsIgnoreCase(name)) {
					return squid_class;
				}
			}
			return null;
		}
	}
}
