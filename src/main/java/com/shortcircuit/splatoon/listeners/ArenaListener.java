package com.shortcircuit.splatoon.listeners;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.Arena;
import com.shortcircuit.splatoon.game.ArenaManager;
import com.shortcircuit.splatoon.game.Match;
import com.shortcircuit.splatoon.game.statistics.LobbySign;
import com.shortcircuit.splatoon.game.timers.RespawnTimer;
import com.shortcircuit.splatoon.player.Inkling;
import com.shortcircuit.splatoon.util.AnnouncementHandler;
import com.shortcircuit.splatoon.util.Utils;
import com.shortcircuit.utils.bukkit.command.PermissionUtils;
import org.bukkit.GameMode;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;

import java.util.Iterator;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class ArenaListener implements Listener {
	private final ArenaManager arena_manager;

	public ArenaListener() {
		arena_manager = Splatoon.getInstance().getArenaManager();
	}

	@EventHandler
	public void noLeave(final PlayerMoveEvent event) {
		if(event.getTo().getWorld() != event.getFrom().getWorld()){
			return;
		}
		if (event.getTo().distanceSquared(event.getFrom()) > 0 &&
				Utils.isPlayerInGame(event.getPlayer()) && !Utils.isInArena(event.getTo(),
				arena_manager.getArenaContainingInkling(event.getPlayer().getUniqueId()))) {
			event.setTo(event.getFrom());
			AnnouncementHandler.announceUserOnly("arena.noleave", event.getPlayer());
		}
	}

	@EventHandler
	public void noLeave(final PlayerTeleportEvent event) {
		if(event.getTo().getWorld() != event.getFrom().getWorld()){
			return;
		}
		if (Utils.isPlayerInGame(event.getPlayer())
				&& event.getTo().distanceSquared(event.getFrom()) > 0
				&& !Utils.isInArena(event.getTo(),
				arena_manager.getArenaContainingInkling(event.getPlayer().getUniqueId()))) {
			event.setTo(event.getFrom());
			AnnouncementHandler.announceUserOnly("arena.noleave", event.getPlayer());
		}
	}

	@EventHandler
	public void noLeave(final PlayerQuitEvent event) {
		if (Utils.isPlayerInGame(event.getPlayer())) {
			Inkling inkling = arena_manager.getInkling(event.getPlayer().getUniqueId());
			Match match = arena_manager.getArenaContainingInkling(inkling.getID()).getMatchHandler().getCurrentMatch();
			match.removeInkling(inkling);
		}
	}

	@EventHandler
	public void noEnter(final PlayerMoveEvent event) {
		if(event.getTo().getWorld() != event.getFrom().getWorld()){
			return;
		}
		if (event.getTo().distanceSquared(event.getFrom()) > 0 && !Utils.isPlayerInMatch(event.getPlayer())
				&& !PermissionUtils.hasPermission(event.getPlayer(), "splatoon.arena.edit")) {
			for (Arena arena : arena_manager.getArenas()) {
				if (Utils.isInArena(event.getTo(), arena)) {
					event.setTo(event.getFrom());
					AnnouncementHandler.announceUserOnly("arena.noentry", event.getPlayer());
					return;
				}
			}
		}
	}

	@EventHandler
	public void noEnter(final PlayerTeleportEvent event) {
		if(event.getTo().getWorld() != event.getFrom().getWorld()){
			return;
		}
		if (event.getTo().distanceSquared(event.getFrom()) > 0 && !Utils.isPlayerInMatch(event.getPlayer())
				&& !PermissionUtils.hasPermission(event.getPlayer(), "splatoon.arena.edit")) {
			for (Arena arena : arena_manager.getArenas()) {
				if (Utils.isInArena(event.getTo(), arena)) {
					event.setTo(event.getFrom());
					AnnouncementHandler.announceUserOnly("arena.noentry", event.getPlayer());
					return;
				}
			}
		}
	}

	@EventHandler
	public void noDamage(final EntityDamageByEntityEvent event) {
		if (event.isCancelled() || !event.getEntityType().equals(EntityType.PLAYER)) {
			return;
		}
		Player player = (Player) event.getEntity();
		Inkling inkling = arena_manager.getInkling(player.getUniqueId());
		if (inkling == null) {
			return;
		}
		if (event.getCause().equals(EntityDamageEvent.DamageCause.PROJECTILE) && event.getDamager() instanceof Snowball) {
			Projectile projectile = (Projectile) event.getDamager();
			if (projectile.getShooter() instanceof Player) {
				Inkling shooter = arena_manager.getInkling(((Player) projectile.getShooter()).getUniqueId());
				if (shooter != null && shooter.getTeam().equals(inkling.getTeam())) {
					event.setCancelled(true);
					return;
				}
			}
			event.setDamage(2.0);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void death(final EntityDamageEvent event) {
		if (!event.getEntityType().equals(EntityType.PLAYER)) {
			return;
		}
		Player player = (Player) event.getEntity();
		Inkling inkling = arena_manager.getInkling(player.getUniqueId());
		if (inkling == null) {
			return;
		}
		if (!arena_manager.getArenaContainingInkling(inkling.getID()).getMatchHandler().isMatchStarted()) {
			event.setCancelled(true);
			return;
		}
		if (player.getHealth() <= event.getFinalDamage()) {
			inkling.respawn();
			event.setDamage(0);
			for (EntityDamageEvent.DamageModifier modifier : EntityDamageEvent.DamageModifier.values()) {
				if (event.isApplicable(modifier)) {
					event.setDamage(modifier, 0);
				}
			}
		}
	}

	@EventHandler
	public void noDamagePlayer(final PlayerDeathEvent event) {
		if (Utils.isPlayerInMatch(event.getEntity())) {
			event.setKeepInventory(true);
			event.setKeepLevel(true);
		}
	}

	@EventHandler
	public void noDamage(final PlayerRespawnEvent event) {
		if (Utils.isPlayerInMatch(event.getPlayer())) {
			Inkling inkling = arena_manager.getInkling(event.getPlayer().getUniqueId());
			Arena arena = arena_manager.getArenaContainingInkling(event.getPlayer().getUniqueId());
			event.setRespawnLocation(arena.getTeamSpawn(inkling.getTeam()));
			inkling.setDead(false);
		}
	}

	/*
	@EventHandler
	public void noEdit(final BlockPistonExtendEvent event){
		System.out.println(event);
		System.out.println(event.getBlock());
	}

	@EventHandler
	public void noEdit(final BlockPistonRetractEvent event){
		System.out.println(event);
		System.out.println(event.getBlock());
	}
	*/

	@EventHandler
	public void noEdit(final BlockBreakEvent event) {
		if (Utils.isInArena(event.getBlock().getLocation())
				&& (!PermissionUtils.hasPermission(event.getPlayer(), "splatoon.arena.edit")
				|| arena_manager.getArenaByLocation(event.getBlock().getLocation()).getMatchHandler().hasMatch())) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void noEdit(final InventoryClickEvent event) {
		if (Utils.isInArena(event.getWhoClicked().getLocation())
				&& arena_manager.getArenaByLocation(event.getWhoClicked().getLocation()).getMatchHandler().hasMatch()) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void noEdit(final BlockPlaceEvent event) {
		if (Utils.isInArena(event.getBlockPlaced().getLocation())
				&& (!PermissionUtils.hasPermission(event.getPlayer(), "splatoon.arena.edit")
				|| arena_manager.getArenaByLocation(event.getBlockPlaced().getLocation()).getMatchHandler().hasMatch())) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void noEdit(final PlayerDropItemEvent event) {
		if (Utils.isInArena(event.getPlayer().getLocation())
				&& (!PermissionUtils.hasPermission(event.getPlayer(), "splatoon.arena.edit")
				|| arena_manager.getArenaByLocation(event.getPlayer().getLocation()).getMatchHandler().hasMatch())) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void noEdit(final PlayerPickupItemEvent event) {
		if (Utils.isInArena(event.getItem().getLocation())
				&& (!PermissionUtils.hasPermission(event.getPlayer(), "splatoon.arena.edit")
				|| arena_manager.getArenaByLocation(event.getItem().getLocation()).getMatchHandler().hasMatch())) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void noGameModeChange(final PlayerGameModeChangeEvent event) {
		Inkling inkling = arena_manager.getInkling(event.getPlayer().getUniqueId());
		if (inkling != null) {
			if ((event.getNewGameMode().equals(GameMode.SURVIVAL) && !inkling.isDead())) {
				return;
			}
			if(RespawnTimer.SPECTATOR != null && event.getNewGameMode().equals(RespawnTimer.SPECTATOR) && inkling.isDead()) {
				return;
			}
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void noFlight(final PlayerToggleFlightEvent event) {
		Inkling inkling = arena_manager.getInkling(event.getPlayer().getUniqueId());
		if (inkling != null && event.isFlying()) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void noHunger(final FoodLevelChangeEvent event) {
		Inkling inkling = arena_manager.getInkling(event.getEntity().getUniqueId());
		if (inkling != null) {
			event.setFoodLevel(20);
		}
	}

	@EventHandler
	public void noConsume(final PlayerItemConsumeEvent event) {
		Inkling inkling = arena_manager.getInkling(event.getPlayer().getUniqueId());
		if (inkling != null) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void joinMatch(final PlayerInteractEvent event) {
		if (event.getPlayer().isSneaking()) {
			return;
		}
		Block clicked = event.getClickedBlock();
		if (clicked == null
				|| !(clicked.getState() instanceof Sign)
				|| !PermissionUtils.hasPermission(event.getPlayer(), "splatoon.match.join")
				|| Utils.isPlayerInMatch(event.getPlayer())) {
			return;
		}
		for (Arena arena : arena_manager.getArenas()) {
			if (clicked.getLocation().equals(arena.getStatusSign().getSignLocation())) {
				if (!arena.isEnabled()) {
					AnnouncementHandler.announceUserOnly("arena.disabled", event.getPlayer());
					event.setCancelled(true);
					return;
				}
				if (!arena.getMatchHandler().hasMatch()) {
					arena.getMatchHandler().newMatch();
				}
				if (!arena.getMatchHandler().isMatchStarted()) {
					Match match = arena.getMatchHandler().getCurrentMatch();
					match.addInkling(event.getPlayer(), match.getDeficientTeam(), match.getDeficientClass());
				}
				event.setCancelled(true);
				return;
			}
			for (LobbySign lobby_sign : arena.getLobbySigns()) {
				if (clicked.getLocation().equals(lobby_sign.getSignLocation())) {
					if (!arena.isEnabled()) {
						AnnouncementHandler.announceUserOnly("arena.disabled", event.getPlayer());
						event.setCancelled(true);
						return;
					}
					if (!arena.getMatchHandler().hasMatch()) {
						arena.getMatchHandler().newMatch();
					}
					if (!arena.getMatchHandler().isMatchStarted()) {
						Match match = arena.getMatchHandler().getCurrentMatch();
						match.addInkling(event.getPlayer(), lobby_sign.getJoinColor(), lobby_sign.getJoinClass());
					}
					event.setCancelled(true);
					return;
				}
			}
		}
	}

	@EventHandler
	public void removeSign(final BlockBreakEvent event) {
		Block broken = event.getBlock();
		for (Arena arena : arena_manager.getArenas()) {
			if (broken.getLocation().equals(arena.getStatusSign().getSignLocation())) {
				System.out.println("MAIN SIGN REMOVED");
				arena.getStatusSign().setSignLocation(null);
				return;
			}
			Iterator<LobbySign> sign_iterator = arena.getLobbySigns().iterator();
			while (sign_iterator.hasNext()) {
				LobbySign sign = sign_iterator.next();
				if (sign.getSignLocation().equals(broken.getLocation())) {
					sign_iterator.remove();
					return;
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void arenaChat(final AsyncPlayerChatEvent event) {
		Player sender = event.getPlayer();
		if (Utils.isPlayerInMatch(sender)) {
			event.getRecipients().clear();
			for (Inkling inkling : arena_manager.getArenaContainingInkling(sender.getUniqueId()).getMatchHandler().getCurrentMatch().getInklings()) {
				event.getRecipients().add(inkling.getPlayer());
			}
			return;
		}
		Iterator<Player> recipients = event.getRecipients().iterator();
		while (recipients.hasNext()) {
			Player recipient = recipients.next();
			if (Utils.isPlayerInMatch(recipient)) {
				recipients.remove();
			}
		}
	}
}
