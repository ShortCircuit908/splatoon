package com.shortcircuit.splatoon.listeners;

import com.shortcircuit.splatoon.Splatoon;
import com.shortcircuit.splatoon.game.Arena;
import com.shortcircuit.splatoon.game.Match;
import com.shortcircuit.splatoon.game.event.PlayerPaintBlockEvent;
import com.shortcircuit.splatoon.game.statistics.MatchStatistics;
import com.shortcircuit.splatoon.player.Inkling;
import com.shortcircuit.splatoon.player.TeamColor;
import com.shortcircuit.splatoon.util.Utils;
import com.shortcircuit.utils.bukkit.BukkitUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
@SuppressWarnings({"deprecation", "unused"})
public class SplatListener implements Listener {
	public static final BlockFace[] FACE_MODIFIERS = new BlockFace[]{BlockFace.DOWN, BlockFace.SELF, BlockFace.UP};
	public static final BlockFace[] CHECK_FACES = new BlockFace[]{BlockFace.SELF, BlockFace.NORTH, BlockFace.NORTH_EAST, BlockFace.EAST, BlockFace.SOUTH_EAST, BlockFace.SOUTH, BlockFace.SOUTH_WEST, BlockFace.WEST, BlockFace.NORTH_WEST};

	public SplatListener() {
	}

	@EventHandler
	public void splat(final ProjectileHitEvent event) {
		if (!(event.getEntityType().equals(EntityType.SNOWBALL)
				|| event.getEntityType().equals(EntityType.ARROW))
				|| !(event.getEntity().getShooter() instanceof Player)) {
			return;
		}
		Player player = (Player) event.getEntity().getShooter();
		Inkling inkling = Splatoon.getInstance().getArenaManager().getInkling(player.getUniqueId());
		if (inkling == null || inkling.isDead()
				|| !Splatoon.getInstance().getArenaManager().getArenaContainingInkling(player.getUniqueId()).getMatchHandler().isMatchStarted()) {
			if (event.getEntityType().equals(EntityType.ARROW)) {
				event.getEntity().remove();
			}
			return;
		}
		Arena arena = Splatoon.getInstance().getArenaManager().getArenaContainingInkling(inkling.getPlayer().getUniqueId());
		if (!Utils.isInArena(event.getEntity().getLocation(), arena)) {
			return;
		}
		MatchStatistics statistics = arena.getMatchHandler().getCurrentMatch().getStatistics();
		Block block = event.getEntity().getLocation().getBlock();
		TeamColor team = inkling.getTeam();
		splat(inkling, arena.getMatchHandler().getCurrentMatch(), block, team, statistics);
		if (event.getEntityType().equals(EntityType.ARROW)) {
			event.getEntity().remove();
		}
	}

	public static void splat(Inkling inkling, Match match, Block origin, TeamColor team, MatchStatistics statistics) {
		byte new_color = team.getColorData();
		for (BlockFace modifier : FACE_MODIFIERS) {
			for (BlockFace face : CHECK_FACES) {
				Block neighbor = BukkitUtils.getNeighboringBlock(origin.getLocation(), modifier, face).getBlock();
				if (neighbor != null && Utils.getPaintableSurfaces().contains(neighbor.getType())) {
					if (!Utils.isColorableSurface(neighbor.getType())) {
						switch (neighbor.getType()) {
							case GLASS:
								neighbor.setType(Material.STAINED_GLASS);
								break;
							case THIN_GLASS:
								neighbor.setType(Material.STAINED_GLASS_PANE);
								break;
							default:
								neighbor.setType(Material.WOOL);
								break;
						}
						neighbor.setData((byte) 0);
					}
					byte old_color = neighbor.getData();
					if (old_color != new_color) {
						neighbor.setData(new_color);
						statistics.increment(team);
						if (TeamColor.getTeam(old_color) != null) {
							statistics.decrement(TeamColor.getTeam(old_color));
						}
						Bukkit.getServer().getPluginManager().callEvent(new PlayerPaintBlockEvent(inkling, match, neighbor, TeamColor.getTeam(old_color), team));
					}
					statistics.update();
					neighbor.getState().update();
				}
			}
		}

	}

	@EventHandler
	public void changeSquid(final PlayerItemHeldEvent event) {
		Player player = event.getPlayer();
		Inkling inkling = Splatoon.getInstance().getArenaManager().getInkling(player.getUniqueId());
		if (inkling == null) {
			return;
		}
		ItemStack item = player.getInventory().getItem(event.getNewSlot());
		if (item == null || item.getType().equals(Material.AIR)) {
			byte team_color = inkling.getTeam().getColorData();
			Block standing_on = inkling.getPlayer().getLocation().subtract(0, 1, 0).getBlock();
			if (standing_on == null
					|| !Utils.getPaintableSurfaces().contains(standing_on.getType())
					|| standing_on.getData() == 0
					|| standing_on.getData() != team_color) {
				return;
			}
			// SQUID TIME
			inkling.doDisguise(Splatoon.getInstance().getArenaManager().getArenaContainingInkling(inkling.getID()).getMatchHandler().getCurrentMatch());
		}
		else {
			// No more squid
			inkling.doUndisguise();
		}
	}

	@EventHandler
	public void changeSquid(final PlayerMoveEvent event) {
		Player player = event.getPlayer();
		Inkling inkling = Splatoon.getInstance().getArenaManager().getInkling(player.getUniqueId());
		if (inkling == null) {
			return;
		}
		byte team_color = inkling.getTeam().getColorData();
		Block standing_on = inkling.getPlayer().getLocation().subtract(0, 1, 0).getBlock();
		if (standing_on == null
				|| !standing_on.getType().isSolid()
				|| (Utils.getPaintableSurfaces().contains(standing_on.getType())
				&& standing_on.getData() == team_color)) {
			return;
		}
		inkling.doUndisguise();
	}
}
