package com.shortcircuit.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 9/29/2015
 */
public class ClassUtils {
	public static StackTraceElement getCallerTrace() {
		return Thread.currentThread().getStackTrace()[3];
	}

	public static StackTraceElement getCallerTrace(int level) {
		return Thread.currentThread().getStackTrace()[level];
	}

	public static <T> T cast(Object obj, Class<T> clazz) {
		if (obj == null) {
			return null;
		}
		try {
			if (obj instanceof Long && !clazz.equals(Long.class)) {
				Long lobj = (Long) obj;
				Integer iobj = lobj.intValue();
				return clazz.cast(iobj);
			}
			return clazz.cast(obj);
		}
		catch (ClassCastException e) {
			if (clazz.equals(String.class)) {
				return (T) obj.toString();
			}
			throw e;
		}
	}

	public static <T> List<T> castList(List<?> list, Class<T> clazz) {
		if (list == null) {
			return null;
		}
		List<T> cast;
		try {
			Class<? extends List> list_class = list.getClass();
			try {
				cast = list_class.getDeclaredConstructor(int.class).newInstance(list.size());
			}
			catch (NoSuchMethodException e) {
				cast = list_class.getDeclaredConstructor().newInstance();
			}
		}
		catch (ReflectiveOperationException e) {
			cast = new ArrayList<>(list.size());
		}
		for (Object obj : list) {
			cast.add(cast(obj, clazz));
		}
		return cast;
	}

	public static <T extends Collection<?>, E, C extends Collection<E>> C castCollection(T collection, Class<E> type_class) {
		return castCollection(collection, type_class, (Class<C>) collection.getClass());
	}

	public static <T extends Collection<?>, E, C extends Collection<E>> C castCollection(T collection, Class<E> type_class, Class<C> set_class) {
		C cast;
		try {
			try {
				cast = set_class.getDeclaredConstructor(int.class).newInstance(collection.size());
			}
			catch (NoSuchMethodException e) {
				cast = set_class.getDeclaredConstructor().newInstance();
			}
		}
		catch (ReflectiveOperationException e) {
			return null;
		}
		for (Object element : collection) {
			cast.add(cast(element, type_class));
		}
		return cast;
	}

	public static String formatStackTrace(StackTraceElement element) {
		return element.getClassName() + "." + element.getMethodName() + "(" + element.getFileName()
				+ ":" + element.getLineNumber() + ")";
	}

	public static String getStackTrace(Throwable throwable) {
		StringWriter writer = new StringWriter();
		throwable.printStackTrace(new PrintWriter(writer));
		return writer.toString();
	}
}
