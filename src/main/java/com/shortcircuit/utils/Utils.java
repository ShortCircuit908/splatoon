package com.shortcircuit.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 10/29/2015
 */
public class Utils {
	/**
	 * Write data directly from an {@link InputStream InputStream} to an {@link OutputStream OutputStream}
	 *
	 * @param src  Source stream
	 * @param dest Destination stream
	 * @throws IOException
	 */
	public static void copyStream(InputStream src, OutputStream dest) throws IOException {
		int n;
		byte[] buffer = new byte[1024];
		while ((n = src.read(buffer)) > 0) {
			dest.write(buffer, 0, n);
		}
		dest.flush();
		src.close();
	}
}
