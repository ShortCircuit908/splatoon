package com.shortcircuit.utils.bukkit;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * A wrapper for a {@link Location Location} for serialization
 *
 * @author ShortCircuit908
 */
public class LocationWrapper implements ConfigurationSerializable {
	private final UUID world;
	private final double x;
	private final double y;
	private final double z;
	private final float yaw;
	private final float pitch;

	public LocationWrapper(Location location) {
		this.world = location.getWorld().getUID();
		this.x = location.getX();
		this.y = location.getY();
		this.z = location.getZ();
		this.yaw = location.getYaw();
		this.pitch = location.getPitch();
	}

	public LocationWrapper(UUID world, double x, double y, double z, float yaw, float pitch) {
		this.world = world;
		this.x = x;
		this.y = y;
		this.z = z;
		this.yaw = yaw;
		this.pitch = pitch;
	}

	public Location getLocation() {
		return new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
	}

	public static LocationWrapper deserialize(Map<String, Object> map) {
		UUID world = UUID.fromString((String) map.get("world"));
		double x = (double) map.get("x");
		double y = (double) map.get("y");
		double z = (double) map.get("z");
		float yaw = (float) (double) map.get("yaw");
		float pitch = (float) (double) map.get("pitch");
		return new LocationWrapper(world, x, y, z, yaw, pitch);
	}

	@Override
	public Map<String, Object> serialize() {
		HashMap<String, Object> map = new HashMap<>();
		map.put("world", world.toString());
		map.put("x", x);
		map.put("y", y);
		map.put("z", z);
		map.put("yaw", yaw);
		map.put("pitch", pitch);
		return map;
	}
}
