package com.shortcircuit.utils.bukkit;

import org.bukkit.Location;
import org.bukkit.block.BlockFace;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 9/29/2015
 */
public class PlayerUtils {
	public static BlockFace getCardinalDirection(Location loc) {
		double rotation = (loc.getYaw() + 180.0) % 360.0;
		if (rotation < 0.0) {
			rotation += 360.0;
		}
		if (0.0 <= rotation && rotation < 22.5) {
			return BlockFace.NORTH;
		}
		else if (22.5 <= rotation && rotation < 67.5) {
			return BlockFace.NORTH_EAST;
		}
		else if (67.5 <= rotation && rotation < 112.5) {
			return BlockFace.EAST;
		}
		else if (112.5 <= rotation && rotation < 157.5) {
			return BlockFace.SOUTH_EAST;
		}
		else if (157.5 <= rotation && rotation < 202.5) {
			return BlockFace.SOUTH;
		}
		else if (202.5 <= rotation && rotation < 247.5) {
			return BlockFace.SOUTH_WEST;
		}
		else if (247.5 <= rotation && rotation < 292.5) {
			return BlockFace.WEST;
		}
		else if (292.5 <= rotation && rotation < 337.5) {
			return BlockFace.NORTH_WEST;
		}
		else if (337.5 <= rotation && rotation < 360.0) {
			return BlockFace.NORTH;
		}
		return null;
	}
}
