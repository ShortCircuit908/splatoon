package com.shortcircuit.utils.bukkit;

import com.shortcircuit.utils.Version;
import com.shortcircuit.utils.bukkit.command.PermissionUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;

import java.io.IOException;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 11/3/2015
 */
public class
CombinationPluginUpdater implements Runnable, Listener {
	private final Plugin plugin;
	private final Integer bukkit_id;
	private final int spigot_id;
	private final String slug;
	private final String api_key;
	private final boolean download;
	private boolean spigot_success = false;
	private BukkitPluginUpdater.Result result = null;

	private CombinationPluginUpdater(Plugin plugin, int spigot_id, Integer bukkit_id, String slug, String api_key, boolean download) {
		this.plugin = plugin;
		this.bukkit_id = bukkit_id;
		this.spigot_id = spigot_id;
		this.slug = slug;
		this.api_key = api_key;
		this.download = download;
		plugin.getServer().getScheduler().scheduleAsyncDelayedTask(plugin, this, 0);
	}

	/**
	 * Create a new plugin updater
	 *
	 * @param plugin    The plugin to update
	 * @param spigot_id The plugin's ID on spigotmc.org
	 * @param slug      The plugin's slug on dev.bukkit.org
	 * @param download  If <code>true</code>, any updates will be automatically downloaded
	 */
	public CombinationPluginUpdater(Plugin plugin, int spigot_id, String slug, boolean download) {
		this(plugin, spigot_id, null, slug, null, download);
	}

	/**
	 * Create a new plugin updater
	 *
	 * @param plugin    The plugin to update
	 * @param spigot_id The plugin's ID on spigotmc.org
	 * @param slug      The plugin's slug on dev.bukkit.org
	 * @param api_key   The developer's API key
	 * @param download  If <code>true</code>, any updates will be automatically downloaded
	 */
	public CombinationPluginUpdater(Plugin plugin, int spigot_id, String slug, String api_key, boolean download) {
		this(plugin, spigot_id, null, slug, api_key, download);
	}

	/**
	 * Create a new plugin updater
	 *
	 * @param plugin    The plugin to update
	 * @param spigot_id The plugin's ID on spigotmc.org
	 * @param bukkit_id The plugin's ID on dev.bukkit.org
	 * @param download  If <code>true</code>, any updates will be automatically downloaded
	 */
	public CombinationPluginUpdater(Plugin plugin, int spigot_id, int bukkit_id, boolean download) {
		this(plugin, spigot_id, bukkit_id, null, null, download);
	}

	/**
	 * Create a new plugin updater
	 *
	 * @param plugin    The plugin to update
	 * @param spigot_id The plugin's ID on spigotmc.org
	 * @param bukkit_id The plugin's ID on dev.bukkit.org
	 * @param api_key   The developer's API key
	 * @param download  If <code>true</code>, any updates will be automatically downloaded
	 */
	public CombinationPluginUpdater(Plugin plugin, int spigot_id, int bukkit_id, String api_key, boolean download) {
		this(plugin, spigot_id, bukkit_id, null, api_key, download);
	}

	@Override
	public void run() {
		try {
			Version latest_version = SpigotPluginUpdater.query(plugin, spigot_id);
			spigot_success = true;
			if (latest_version != null) {
				result = new BukkitPluginUpdater.Result(BukkitPluginUpdater.ResultState.UPDATE_AVAILABLE, latest_version,
						new BukkitPluginUpdater.UpdateInfo(null, plugin.getName() + " v" + latest_version, spigot_id));
			}
		}
		catch (IOException e) {
			if (bukkit_id != null) {
				result = BukkitPluginUpdater.query(plugin, bukkit_id, api_key, download);
			}
			else {
				result = BukkitPluginUpdater.query(plugin, slug, api_key, download);
			}
			if(result.getException() != null){
				plugin.getLogger().warning("Failed to check Bukkit for updates:");
				plugin.getLogger().warning(result.getException().getClass().getCanonicalName() + ": "
						+ result.getException().getMessage());
			}
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void notify(final PlayerJoinEvent event) {
		Player player = event.getPlayer();
		if (result != null && PermissionUtils.hasPermission(player, "updater.notify")) {
			switch (result.getState()) {
				case INFO_FETCH_FAILED:
				case FAILED:
				case KEY_REJECTED:
				case INVALID_FORMAT:
					player.sendMessage(ChatColor.RED + String.format("[%1$s] Failed to check Bukkit for updates. Please check " +
							"the console for more information.", plugin.getName()));
					if (result.getException() != null) {
						player.sendMessage(ChatColor.RED + String.format("[%1$s] %2$s: %3$s", plugin.getName(),
								result.getException().getClass().getCanonicalName(), result.getException().getMessage()));
					}
					break;
				case UPDATE_DOWNLOADED:
					player.sendMessage(ChatColor.AQUA + String.format("[%1$s] An update has been found: %2$s %3$s for %4$s",
							plugin.getName(), result.getLatestVersionInfo().getReleaseType(), result.getLatestVersionInfo().getName(),
							result.getLatestVersionInfo().getGameVersion()));
					switch (result.getDownloadResult().getState()) {
						case DOWNLOAD_SUCCESS:
							player.sendMessage(ChatColor.AQUA + String.format("[%1$s] Please restart the server to apply the update",
									plugin.getName()));
							break;
						case DOWNLOAD_FAIL:
							player.sendMessage(ChatColor.RED + String.format("[%1$s] Failed to download update. " +
									"Please check the console for more information.", plugin.getName()));
							player.sendMessage(ChatColor.RED + String.format("[%1$s] %2$s: %3$s", plugin.getName(),
									result.getDownloadResult().getException().getClass().getCanonicalName(),
									result.getDownloadResult().getException().getMessage()));
							break;
						case BAD_CHECKSUM:
							player.sendMessage(ChatColor.RED + String.format("[%1$s] Downloaded update is corrupted",
									plugin.getName()));
							player.sendMessage(ChatColor.RED + String.format("[%1$s] Please download the file manually at %2$s",
									plugin.getName(), result.getLatestVersionInfo().getDownloadUrl()));
							break;
					}
					break;
				case NOT_UPDATED:
					player.sendMessage(ChatColor.AQUA + String.format("[%1$s] The plugin has not been updated", plugin.getName()));
					break;
				case UPDATE_AVAILABLE:
					if (!spigot_success) {
						player.sendMessage(ChatColor.AQUA + String.format("[%1$s] An update has been found: %2$s %3$s for %4$s",
								plugin.getName(), result.getLatestVersionInfo().getReleaseType(), result.getLatestVersionInfo().getName(),
								result.getLatestVersionInfo().getGameVersion()));
					}
					else {
						player.sendMessage(ChatColor.AQUA + String.format("[%1$s] An update has been found: %2$s",
								plugin.getName(), result.getLatestVersionInfo().getName()));
					}
					player.sendMessage(ChatColor.AQUA + String.format("[%1$s] Download the update at %2$s", plugin.getName(),
							result.getLatestVersionInfo().getDownloadUrl()));
					break;
				case UP_TO_DATE:
					player.sendMessage(ChatColor.AQUA + String.format("[%1$s] Plugin is up-to-date", plugin.getName()));
					break;
				case NEWER_VERSION:
					player.sendMessage(ChatColor.AQUA + String.format("[%1$s] Plugin version is newer than released updates", plugin.getName()));
					break;
			}
		}
	}
}
