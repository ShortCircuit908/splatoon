package com.shortcircuit.utils.bukkit.command;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.plugin.Plugin;
import org.reflections.Reflections;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 10/30/2015
 */
public class CommandRegister {
	private static final LinkedList<BaseCommand> register_queue = new LinkedList<>();
	private static final LinkedList<BaseCommand> unregister_queue = new LinkedList<>();
	private static LateCommandRegisterTask late_register_task;
	private static CommandMap command_map = null;

	static {
		getBukkitCommandMap();
	}

	/**
	 * Designate a plugin to handle late command registration
	 *
	 * @param plugin The plugin to use
	 */
	public static synchronized void delegateLateRegistration(Plugin plugin) {
		if (!plugin.isEnabled()) {
			plugin.getLogger().warning("Cannot delegate late registration to disabled plugin");
		}
		if (late_register_task != null) {
			plugin.getLogger().info("Late registration has already been delegated");
			return;
		}
		late_register_task = new LateCommandRegisterTask();
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, late_register_task, 0);
	}

	/**
	 * Register a command with Bukkit
	 *
	 * @param command The command to register
	 * @return <code>true</code> if the command was successfully registered
	 */
	public static synchronized boolean register(BaseCommand command) {
		command.getPlugin().getLogger().info("Registering command: " + command.getClass().getCanonicalName());
		CommandMap command_map = getBukkitCommandMap();
		if (command_map == null) {
			command.getPlugin().getLogger().warning("Could not access server command map");
			command.getPlugin().getLogger().warning("Command will be queued for later registration");
			synchronized (register_queue) {
				register_queue.add(command);
			}
			return false;
		}
		return command_map.register(command.getPlugin().getName().replaceAll("\\s", ""), command);
	}

	/**
	 * Register all commands in a package with Bukkit
	 * <p/>
	 * The provided plugin's class is used to scan for any classes which extend
	 * {@link BaseCommand BaseCommand}. When a class is found,
	 * it is instantiated with the provided plugin and registered with {@link #register(BaseCommand)}
	 *
	 * @param plugin       The commands' owner
	 * @param package_name The name of the package to scan
	 */
	public static synchronized void registerAll(Plugin plugin, String package_name) {
		plugin.getLogger().info("Scanning for commands in package: " + package_name);
		Reflections reflections = new Reflections(package_name, plugin.getClass().getClassLoader());
		Set<Class<? extends BaseCommand>> command_classes = reflections.getSubTypesOf(BaseCommand.class);
		for (Class<? extends BaseCommand> command_class : command_classes) {
			Constructor<? extends BaseCommand> ctor;
			try {
				try {
					ctor = command_class.getDeclaredConstructor(Plugin.class);
					ctor.setAccessible(true);
				}
				catch (NoSuchMethodException e) {
					ctor = command_class.getDeclaredConstructor(plugin.getClass());
					ctor.setAccessible(true);
				}
			}
			catch (ReflectiveOperationException e) {
				plugin.getLogger().warning("Command class " + command_class.getCanonicalName() + " does not have a valid constructor");
				continue;
			}
			BaseCommand command;
			try {
				command = ctor.newInstance(plugin);
			}
			catch (ReflectiveOperationException e) {
				plugin.getLogger().warning("Could not instantiate command: " + command_class.getCanonicalName());
				plugin.getLogger().warning(e.getClass().getCanonicalName() + ": " + e.getMessage());
				continue;
			}
			register(command);
		}
	}

	/**
	 * Unregister a command with Bukkit
	 *
	 * @param command The command to unregister
	 * @return <code>true</code> if the command was successfully unregistered
	 */
	public static boolean unregister(BaseCommand command) {
		command.getPlugin().getLogger().info("Unregistering command: " + command.getClass().getCanonicalName());
		CommandMap command_map = getBukkitCommandMap();
		if (command_map == null) {
			command.getPlugin().getLogger().warning("Could not access server command map");
			command.getPlugin().getLogger().warning("Command will be queued for later unregistration");
			synchronized (unregister_queue) {
				unregister_queue.add(command);
			}
			return false;
		}
		return command.unregister(command_map);
	}

	private static CommandMap getBukkitCommandMap() {
		if (command_map == null) {
			try {
				Field field = Bukkit.getServer().getPluginManager().getClass().getDeclaredField("commandMap");
				field.setAccessible(true);
				command_map = (CommandMap) field.get(Bukkit.getServer().getPluginManager());
			}
			catch (ReflectiveOperationException e) {
				return null;
			}
		}
		return command_map;
	}

	private static class LateCommandRegisterTask implements Runnable {
		@Override
		public void run() {
			synchronized (register_queue) {
				Iterator<BaseCommand> iterator = register_queue.iterator();
				register_queue.clear();
				while (iterator.hasNext()) {
					register(iterator.next());
				}
				iterator = unregister_queue.iterator();
				unregister_queue.clear();
				while (iterator.hasNext()) {
					unregister(iterator.next());
				}
			}
			late_register_task = null;
		}
	}
}
