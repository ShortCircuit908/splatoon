package com.shortcircuit.utils.bukkit.command.exceptions;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class ConsoleOnlyException extends CommandException {
	public ConsoleOnlyException() {
		super("This command is console-only");
	}
}
