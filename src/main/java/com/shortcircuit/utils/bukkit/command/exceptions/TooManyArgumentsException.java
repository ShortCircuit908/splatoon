package com.shortcircuit.utils.bukkit.command.exceptions;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 */
public class TooManyArgumentsException extends CommandException {
	public TooManyArgumentsException() {
		super("Too many arguments");
	}
}
