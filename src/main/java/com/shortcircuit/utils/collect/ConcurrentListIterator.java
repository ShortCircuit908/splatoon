package com.shortcircuit.utils.collect;

import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.
 *
 * @author ShortCircuit908
 *         Created on 11/3/2015
 */
public class ConcurrentListIterator<E> implements ListIterator<E> {
	private final ConcurrentArrayList<E> list;
	private int index = 0;
	private int size = 0;
	private int last_index = -1;


	protected ConcurrentListIterator(ConcurrentArrayList<E> list, int pos) {
		this.list = list;
		size = list.size();
	}

	@Override
	public synchronized boolean hasNext() {
		synchronized (this) {
			return index < size;
		}
	}

	@Override
	public synchronized E next() {
		synchronized (this) {
			if (index >= size) {
				throw new NoSuchElementException();
			}
			last_index = index;
			return list.get(index++);
		}
	}

	@Override
	public synchronized boolean hasPrevious() {
		synchronized (this) {
			return index > 0;
		}
	}

	@Override
	public synchronized E previous() {
		synchronized (this) {
			if (index <= 0) {
				throw new NoSuchElementException();
			}
			last_index = index;
			return list.get(--index);
		}
	}

	@Override
	public synchronized int nextIndex() {
		synchronized (this) {
			return index + 1;
		}
	}

	@Override
	public synchronized int previousIndex() {
		synchronized (this) {
			return index - 1;
		}
	}

	@Override
	public synchronized void remove() {
		synchronized (this) {
			int i = last_index - 1;
			if (i < 0 || i >= size) {
				throw new IllegalStateException();
			}
			list.remove(i);
			index = i;
			size = list.size();
		}
	}

	@Override
	public synchronized void set(E e) {
		synchronized (this) {
			if (last_index < 0) {
				throw new IllegalStateException();
			}
			list.set(last_index, e);
		}
	}

	@Override
	public void add(E e) {
		synchronized (this) {
			list.add(index++, e);
			last_index = -1;
		}
	}
}
